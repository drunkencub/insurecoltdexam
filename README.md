# [WIP] Embla Exam SSE/TL
App for testing capabilities of SSE/TL upon recruitment

This is a sample API for the questions in the Insurance Co Ltd. Case study. 

This is written in Python (why would anyone use any other language?)
But same principles can be adopted in any language/framework

This is also setup to deploy to AWS lambda through Zappa as a serverless app.

(c) Chathuranga Bandara 

